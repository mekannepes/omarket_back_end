package apiserver

import (
	"net/http"
	"omarket/internal/app/model"
	"omarket/internal/app/store"

	"log"
	"time"

	jwt "github.com/appleboy/gin-jwt/v2"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

var (
	identityKey = "id"
)

type server struct {
	router *gin.Engine
	logger *logrus.Logger
	store  store.Store
}

type AuthMagazin struct {
	ID string
}

func newServer(store store.Store) *server {
	s := &server{
		router: gin.New(),
		logger: logrus.New(),
		store:  store,
	}
	s.configureRouter()
	return s
}

func (s *server) configureRouter() {

	// the jwt middleware
	authMiddleware, err := jwt.New(&jwt.GinJWTMiddleware{
		Realm:       "test zone",
		Key:         []byte("12yui9]op45678dfghjert0a[;'2bzxcv5kl9343n876wms,./"),
		Timeout:     time.Hour,
		MaxRefresh:  time.Hour,
		IdentityKey: identityKey,
		PayloadFunc: func(data interface{}) jwt.MapClaims {
			if v, ok := data.(*AuthMagazin); ok {
				return jwt.MapClaims{
					identityKey: v.ID,
				}
			}
			return jwt.MapClaims{}
		},
		IdentityHandler: func(c *gin.Context) interface{} {
			claims := jwt.ExtractClaims(c)
			return &AuthMagazin{
				ID: claims[identityKey].(string),
			}
		},
		Authenticator: func(c *gin.Context) (interface{}, error) {
			var loginVals model.Magazin
			if err := c.ShouldBind(&loginVals); err != nil {
				return "", jwt.ErrMissingLoginValues
			}
			userEmail := loginVals.Email
			password := loginVals.Password

			magazin, err := s.store.User().FindByEmail(userEmail)
			if err != nil {
				return nil, jwt.ErrFailedAuthentication
			}
			if userEmail == magazin.Email && password == magazin.Password {
				return &AuthMagazin{
					ID: magazin.ID.Hex(),
				}, nil
			}

			return nil, jwt.ErrFailedAuthentication
		},
		Authorizator: func(data interface{}, c *gin.Context) bool {
			magazin, ok := data.(*AuthMagazin)
			okMagazin := s.store.User().FindById(magazin.ID)
			if ok && okMagazin {
				return true
			}
			return false
		},
		Unauthorized: func(c *gin.Context, code int, message string) {
			c.JSON(code, gin.H{
				"code":    code,
				"message": message,
			})
		},
		// TokenLookup is a string in the form of "<source>:<name>" that is used
		// to extract token from the request.
		// Optional. Default value "header:Authorization".
		// Possible values:
		// - "header:<name>"
		// - "query:<name>"
		// - "cookie:<name>"
		// - "param:<name>"
		TokenLookup: "header: Authorization, query: token, cookie: jwt",
		// TokenLookup: "query:token",
		// TokenLookup: "cookie:token",

		// TokenHeadName is a string in the header. Default value is "Bearer"
		TokenHeadName: "Bearer",

		// TimeFunc provides the current time. You can override it to use another time value. This is useful for testing or if your server uses a different time zone than your tokens.
		TimeFunc: time.Now,
	})

	if err != nil {
		log.Fatal("JWT Error:" + err.Error())
	}
	s.router.Use(cors.Default())
	s.router.POST("/login", authMiddleware.LoginHandler)

	s.router.NoRoute(authMiddleware.MiddlewareFunc(), func(c *gin.Context) {
		claims := jwt.ExtractClaims(c)
		log.Printf("NoRoute claims: %#v\n", claims)
		c.JSON(404, gin.H{"code": "PAGE_NOT_FOUND", "message": "Page not found"})
	})

	auth := s.router.Group("/api")
	// Refresh time can be longer than token timeout
	auth.GET("/refresh_token", authMiddleware.RefreshHandler)
	auth.Use(authMiddleware.MiddlewareFunc())
	{
		auth.GET("/hello", s.helloHandler)
	}

	//s.router.POST("/user", s.handleMagazinCreate())
}

/*
	Handlers
	/users -> Get; Create user
*/
func (s *server) helloHandler(c *gin.Context) {
	claims := jwt.ExtractClaims(c)
	user, _ := c.Get(identityKey)
	c.JSON(200, gin.H{
		"userID":   claims[identityKey],
		"userName": user.(*AuthMagazin).ID,
		"text":     "Hello World.",
	})
}

//handleMagazinCreate ...
func (s *server) handleMagazinCreate() gin.HandlerFunc {

	type request struct {
		Email    string `json:"email" form:"email"`
		Password string `json:"password" form:"password"`
	}
	return func(c *gin.Context) {
		var req request
		if err := c.ShouldBindJSON(&req); err != nil {
			s.error(c, http.StatusBadRequest, err)
			return
		}
		user := &model.Magazin{
			Email:    req.Email,
			Password: req.Password,
		}
		if err := s.store.User().Create(user); err != nil {
			s.error(c, http.StatusUnprocessableEntity, err)
			return
		}
		s.respond(c, http.StatusCreated, user)
	}
}

func (s *server) error(c *gin.Context, code int, err error) {
	s.respond(c, code, map[string]string{"error": err.Error()})
}

func (s *server) respond(c *gin.Context, code int, date interface{}) {
	c.JSON(code, date)
}
