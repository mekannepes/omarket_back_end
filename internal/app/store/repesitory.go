package store

import "omarket/internal/app/model"

type MagazinRepository interface {
	Create(*model.Magazin) error
	Find(int) (*model.Magazin, error)
	FindByEmail(string) (*model.Magazin, error)
	FindById(string) bool
}
