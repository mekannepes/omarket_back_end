package sqlstore

import (
	"context"
	"log"
	"omarket/internal/app/model"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

//MagazinRepository ...
type MagazinRepository struct {
	store *Store
}

// Create ...
func (r *MagazinRepository) Create(u *model.Magazin) error {

	return nil
}

// Find ...
func (r *MagazinRepository) Find(id int) (*model.Magazin, error) {

	u := &model.Magazin{}
	return u, nil
}

// FindByEmail ...
func (r *MagazinRepository) FindByEmail(email string) (*model.Magazin, error) {

	collection := r.store.db.Database("omarket").Collection("store")
	filter := bson.M{"login": email}

	ctx, _ := context.WithTimeout(context.Background(), 2*time.Second)
	magazine := &model.Magazin{}
	err := collection.FindOne(ctx, filter).Decode(&magazine)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	return magazine, nil
}

//FindById
func (r *MagazinRepository) FindById(id string) bool {
	collection := r.store.db.Database("omarket").Collection("store")

	objID, _ := primitive.ObjectIDFromHex(id)
	filter := bson.M{"_id": objID}

	ctx, _ := context.WithTimeout(context.Background(), 2*time.Second)
	magazine := &model.Magazin{}
	err := collection.FindOne(ctx, filter).Decode(&magazine)
	if err != nil {
		log.Println(err)
		return false
	}
	return true
}
