package sqlstore

import (
	"omarket/internal/app/store"

	"go.mongodb.org/mongo-driver/mongo"
)

//Store ...
type Store struct {
	db                *mongo.Client
	magazinRepository *MagazinRepository
}

//New ...
func New(client *mongo.Client) *Store {
	return &Store{
		db: client,
	}
}

//User ...
func (s *Store) User() store.MagazinRepository {
	if s.magazinRepository != nil {
		return s.magazinRepository
	}
	s.magazinRepository = &MagazinRepository{
		store: s,
	}
	return s.magazinRepository
}
