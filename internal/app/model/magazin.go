package model

import (
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"golang.org/x/crypto/bcrypt"
)

// Magazin ...
type Magazin struct {
	ID                primitive.ObjectID `json:"id" bson:"_id"`
	Name              string             `json:"name" bson:"name"`
	Address           string             `json:"address" bson:"address"`
	Phone             string             `json:"phone" bson:"phone"`
	Shopkeeper        string             `json:"shopkeeper" bson:"shopkeeper"`
	ShopkeeperPhone   string             `json:"shopkeeper_phone" bson:"shopkeeper_phone"`
	ImageAddress      string             `json:"image_address" bson:"image_address"`
	Categories        []string           `json:"categories" bson:"categories"`
	CountGoods        float64            `json:"count_goods" bson:"count_goods"`
	Sum               float64            `json:"sum" bson:"sum"`
	SumWithDiscount   float64            `json:"sum_with_discount" bson:"sum_with_discount"`
	Email             string             `json:"email" bson:"login"`
	Password          string             `json:"password,omitempty" bson:"password"`
	EncryptedPassword string             `json:"-" bson:"-"`
}

// Validate ...
func (u *Magazin) Validate() error {
	return validation.ValidateStruct(
		u,
		validation.Field(&u.Email, validation.Required, is.Email),
		validation.Field(&u.Password, validation.Required, validation.Length(6, 20)),
	)
}

// BeforeCreate ...
func (u *Magazin) BeforeCreate() error {
	if len(u.Password) > 0 {
		enc, err := encryptString(u.Password)
		if err != nil {
			return err
		}

		u.EncryptedPassword = enc
	}

	return nil
}

func encryptString(s string) (string, error) {
	b, err := bcrypt.GenerateFromPassword([]byte(s), bcrypt.MinCost)
	if err != nil {
		return "", err
	}

	return string(b), nil
}
