package apiserver

import (
	"omarket/internal/app/store/sqlstore"
	"time"

	"context"

	_ "github.com/lib/pq"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

func Start(config *Config) error {

	client, err := NewDB(config.DatabaseURL)
	if err != nil {
		return err
	}
	defer client.Disconnect(context.TODO())
	store := sqlstore.New(client)
	server := newServer(store)

	if err := server.router.Run(); err != nil {
		return err
	}
	return nil
}

func NewDB(databaseURL string) (*mongo.Client, error) {

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(databaseURL))

	if err != nil {
		return nil, err
	}
	ctx, _ = context.WithTimeout(context.Background(), 2*time.Second)
	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		return nil, err
	}

	return client, nil

}
